package Pieces;

import Game.Color;
import Game.Coordinate;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class PawnTest {

    @Test
    public void isValidMovement() {
        Piece pawn = new Pawn(Color.White, new Coordinate(1, 6));
        final boolean validMovement = pawn.isValidMovement(new Coordinate(1, 5), Color.None);
        assertEquals(true, validMovement);
    }
}