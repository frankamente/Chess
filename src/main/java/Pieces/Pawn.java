package Pieces;

import Game.Color;
import Game.Coordinate;

import java.util.HashMap;

public class Pawn extends Piece {

    private final String value = "P";

    private final Coordinate initialCoordinate;

    public Pawn(Color color, Coordinate coordinate) {
        super(color, coordinate);
        initialCoordinate = coordinate;
    }

    @Override
    public String getValue() {
        return value + "-" + color;
    }

    @Override
    public boolean isValidMovement(Coordinate newCoordinate, Color newColor) {
        final boolean verticalMovement = isValidVerticalMovement(newCoordinate);
        final boolean diagonalMovement = isValidDiagonalMovement(newCoordinate, newColor);
        return verticalMovement || diagonalMovement;
    }

    private boolean isValidDiagonalMovement(Coordinate newCoordinate, Color newColor) {
        if (newColor == Color.None) {
            return false;
        }
        switch (color) {
            case White:
                return (newCoordinate.getX() == coordinate.getX() + 1 &&
                        newCoordinate.getY() == coordinate.getY() - 1) ||
                        (newCoordinate.getX() == coordinate.getX() - 1 &&
                                newCoordinate.getY() == coordinate.getY() - 1);
            case Black:
                return (newCoordinate.getX() == coordinate.getX() + 1 &&
                        newCoordinate.getY() == coordinate.getY() + 1) ||
                        (newCoordinate.getX() == coordinate.getX() - 1 &&
                                newCoordinate.getY() == coordinate.getY() + 1);
        }
        return false;
    }

    @Override
    public boolean thereIsSomethingBetween(Piece piece, Coordinate newCoordinate, HashMap<Coordinate, Piece> pieces) {
        if (piece.getColor() == Color.White) {
            return newCoordinate.getX() == coordinate.getX() &&
                    newCoordinate.getY() == coordinate.getY() - 2 &&
                    pieces.get(new Coordinate(coordinate.getX(), coordinate.getY() - 1)) != null;
        } else if (piece.getColor() == Color.Black) {

            return newCoordinate.getX() == coordinate.getX() &&
                    newCoordinate.getY() == coordinate.getY() + 2 &&
                    pieces.get(new Coordinate(coordinate.getX(), coordinate.getY() + 1)) != null;
        }
        return false;
    }

    private boolean isValidVerticalMovement(Coordinate newCoordinate) {
        if (coordinate.getX() != newCoordinate.getX()) {
            return false;
        }
        switch (color) {
            case White:
                if (coordinate.equals(initialCoordinate)) {
                    if (newCoordinate.getY() == coordinate.getY() - 2) {
                        System.out.println("Valid VerticalMovement");
                        return true;
                    }
                }
                if (newCoordinate.getY() == coordinate.getY() - 1) {
                    System.out.println("Valid VerticalMovement");
                    return true;
                }
                break;
            case Black:
                if (coordinate.equals(initialCoordinate)) {
                    if (newCoordinate.getY() == coordinate.getY() + 2) {
                        System.out.println("Valid VerticalMovement");
                        return true;
                    }
                }
                if (newCoordinate.getY() == coordinate.getY() + 1) {
                    System.out.println("Valid VerticalMovement");
                    return true;
                }
                break;
        }
        return false;
    }
}
