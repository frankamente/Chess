package Pieces;

import Game.*;

import java.util.HashMap;

public class Bishop extends Piece {

    private final String VALUE = "B";

    public Bishop(Color color, Coordinate coordinate) {
        super(color, coordinate);
    }

    @Override
    public String getValue() {
        return VALUE + "-" + color;
    }

    @Override
    public boolean isValidMovement(Coordinate newCoordinate, Color newColor) {
        return false;
    }

    @Override
    public boolean thereIsSomethingBetween(Piece piece, Coordinate newCoordinate, HashMap<Coordinate, Piece> pieces) {
        return false;
    }
}
