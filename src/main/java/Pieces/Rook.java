package Pieces;

import Game.Color;
import Game.Coordinate;

import java.util.HashMap;

public class Rook extends Piece {


    private final String VALUE = "R";

    public Rook(Color color, Coordinate coordinate) {
        super(color, coordinate);
    }


    @Override
    public String getValue() {
        return VALUE + "-" + color;
    }

    @Override
    public boolean isValidMovement(Coordinate newCoordinate, Color newColor) {
        return false;
    }

    @Override
    public boolean thereIsSomethingBetween(Piece piece, Coordinate newCoordinate, HashMap<Coordinate, Piece> pieces) {
        return false;
    }
}
