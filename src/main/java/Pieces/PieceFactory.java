package Pieces;

import Game.Color;
import Game.Coordinate;

import java.util.HashMap;

import static Game.Board.BOARD_DIMENSION;

public class PieceFactory {

    private final HashMap<Coordinate, Piece> pieces;

    public PieceFactory(HashMap<Coordinate, Piece> pieces) {
        this.pieces = pieces;
    }

    public void createPieces() {
        createPawns();
        createHorses();
        createRooks();
        createBishops();
        createKings();
        createQueens();
    }

    private void createBishops() {
        createBishop(Color.Black, 2, 0);
        createBishop(Color.Black, 5, 0);
        createBishop(Color.White, 2, 7);
        createBishop(Color.White, 5, 7);
    }

    private void createBishop(Color color, int x, int y) {
        Coordinate coordinate = new Coordinate(x, y);
        pieces.put(coordinate, new Bishop(color, coordinate));
    }

    private void createRooks() {
        createRook(Color.Black, 0, 0);
        createRook(Color.Black, 7, 0);
        createRook(Color.White, 0, 7);
        createRook(Color.White, 7, 7);
    }

    private void createRook(Color color, int x, int y) {
        Coordinate coordinate = new Coordinate(x, y);
        pieces.put(coordinate, new Rook(color, coordinate));
    }

    private void createQueens() {
        createQueen(Color.Black, 3, 0);
        createQueen(Color.White, 3, 7);
    }

    private void createQueen(Color color, int x, int y) {
        Coordinate coordinate = new Coordinate(x, y);
        pieces.put(coordinate, new Queen(color, coordinate));
    }

    private void createKings() {
        createKings(Color.Black, 4, 0);
        createKings(Color.White, 4, 7);
    }

    private void createKings(Color color, int x, int y) {
        Coordinate coordinate = new Coordinate(x, y);
        pieces.put(coordinate, new King(color, coordinate));
    }

    private void createHorses() {
        createHorse(Color.Black, 1, 0);
        createHorse(Color.Black, 6, 0);
        createHorse(Color.Black, 1, 7);
        createHorse(Color.Black, 6, 7);
    }

    private void createHorse(Color color, int x, int y) {
        Coordinate coordinate = new Coordinate(x, y);
        pieces.put(coordinate, new Horse(color, coordinate));
    }

    private void createPawns() {
        for (int i = 0; i < BOARD_DIMENSION; i++) {
            createPawn(Color.Black, i, 1);
            createPawn(Color.White, i, 6);
        }
    }

    private void createPawn(Color color, int x, int y) {
        Coordinate coordinate = new Coordinate(x, y);
        pieces.put(coordinate, new Pawn(color, coordinate));
    }
}
