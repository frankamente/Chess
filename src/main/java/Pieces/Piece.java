package Pieces;

import Game.*;

import java.util.HashMap;

public abstract class Piece {

    protected final Color color;

    protected Coordinate coordinate;

    public Piece(Color color, Coordinate coordinate) {
        this.color = color;
        this.coordinate = coordinate;
    }

    public Color getColor() {
        return color;
    }


    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void changeCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }
    public abstract String getValue();

    public abstract boolean isValidMovement(Coordinate newCoordinate, Color newColor);

    public abstract boolean thereIsSomethingBetween(Piece piece, Coordinate newCoordinate, HashMap<Coordinate, Piece> pieces);
}
