package Game;

import Pieces.*;

import java.util.HashMap;

public class Board {

    public static final int BOARD_DIMENSION = 8;

    private HashMap<Coordinate, Piece> pieces;

    private PieceFactory piecesFactory;

    private final Player player;

    public Board() {
        pieces = new HashMap<Coordinate, Piece>();
        player = new Player();
        piecesFactory = new PieceFactory(pieces);
    }

    public void initialize() {
        piecesFactory.createPieces();

    }



    public void showBoard() {
        showColumnIndex();
        for (int i = 0; i < BOARD_DIMENSION; i++) {
            showRowIndex(i);
            for (int j = 0; j < BOARD_DIMENSION; j++) {
                final Coordinate coordinate = new Coordinate(j, i);

                final Piece piece = pieces.get(coordinate);
                System.out.print(piece != null ? piece.getValue() : Color.None);
                System.out.print("  ");
            }
            System.out.println();
        }
    }

    private void showRowIndex(int i) {
        System.out.print(i);
        System.out.print("  ");
    }

    private void showColumnIndex() {
        for (int i = 0; i < BOARD_DIMENSION; i++) {
            System.out.print("    ");
            System.out.print(i);
        }
        System.out.println();
    }

    private void blacksMove() {
        System.out.println("Move Blacks");
        move(Color.Black);
    }

    private void move(Color color) {
        Piece piece;
        Coordinate oldCoordinate;
        do {
            oldCoordinate = player.orderCoordinate();
            piece = getPiece(oldCoordinate);
        } while (!sameColor(piece, color));

        Coordinate newCoordinate;
        Color newColor;
        boolean isValidColor;
        boolean isValidMovement;
        boolean thereIsSomethingBetween;
        do {
            isValidColor = true;
            isValidMovement = true;
            thereIsSomethingBetween = false;
            newCoordinate = player.orderNewCoordinate();
            newColor = getColor(newCoordinate);

            if (sameColor(getPiece(newCoordinate), color)) {
                System.out.println("Can't move to the same color");
                isValidColor = false;
            }

            if (!piece.isValidMovement(newCoordinate, newColor)) {
                System.out.println("This Piece can't do this movement");
                isValidMovement = false;
            }

            if (thereIsSomethingBetween(piece, newCoordinate)) {
                System.out.println("thereIsSomethingBetween");
                thereIsSomethingBetween = true;
            }
        }
        while (!isValidColor || !isValidMovement || thereIsSomethingBetween);
        System.out.println("Valid Movement");
        movePiece(piece, oldCoordinate, newCoordinate);
    }

    private void movePiece(Piece piece, Coordinate oldCoordinate, Coordinate newCoordinate) {
        piece.changeCoordinate(newCoordinate);
        pieces.put(newCoordinate, piece);
        pieces.remove(oldCoordinate, piece);
    }

    private void whitesMove() {

        System.out.println("Move Whites");
        move(Color.White);
    }

    private boolean thereIsSomethingBetween(Piece piece, Coordinate newCoordinate) {
        return piece.thereIsSomethingBetween(piece, newCoordinate, pieces);
    }

    private Color getColor(Coordinate coordinate) {
        final Piece piece = pieces.get(coordinate);
        return piece != null ? piece.getColor() : Color.None;
    }

    private boolean sameColor(Piece piece, Color color) {
        return piece != null && color == piece.getColor();
    }

    private Piece getPiece(Coordinate coordinate) {
        return pieces.get(coordinate);
    }

    private boolean thereIsCheckMate() {
        return false;
    }


    public static void main(String[] args) {
        Board board = new Board();
        board.initialize();
        do {
            board.showBoard();
            board.whitesMove();
            board.showBoard();
            board.blacksMove();
        } while (!board.thereIsCheckMate());

    }

}
