package Game;

import Game.Board;
import Game.Coordinate;
import Game.IO;

public class Player {

    public Coordinate orderCoordinate() {

        return new Coordinate(orderPosition("X"), orderPosition("Y"));
    }

    private int orderPosition(String title) {
        IO io = new IO();
        int position;
        do {
            position = io.readInt(String.format("Enter %s Game.Coordinate: ", title));
        } while (!(position >= 0 && position < Board.BOARD_DIMENSION));
        return position;
    }

    public Coordinate orderNewCoordinate() {
        return new Coordinate(orderPosition("new X"), orderPosition("new Y"));
    }
}
