package Game;

public enum Color {
    White("W"),
    Black("B"),
    None("---");

    private String color;

    Color(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return color;
    }

}
